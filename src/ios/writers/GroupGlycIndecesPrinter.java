/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ios.writers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import xglycscan.groupindecesacc.GroupIndeces;

/**
 *
 * @author paiyeta1
 */
public class GroupGlycIndecesPrinter {

    public void print(ArrayList<GroupIndeces> iDIndeces, String output) {
        PrintWriter printer = null;
        
        try {
            //String output = outputDir + File.separator + new Date().getTime() + ".identification.indeces";
            printer = new PrintWriter(new File(output));
            // print header
            /*
             * File(Sample) name
             * Identified peptides
             * Identified glycosites
             * Unique peptides
             * Unique glycosites
             * Specificity
             * 
             */
            printer.println("File(Sample) name" + "\t" + 
                            "Identified peptides" + "\t" + 
                            "Identified glycosites" + "\t" + 
                            "Unique peptides" + "\t" + 
                            "Unique glycosites"  + "\t" + 
                            "Specificity");
            // print respective file identification indeces
            Iterator<GroupIndeces> itr = iDIndeces.iterator();
            while(itr.hasNext()){
                GroupIndeces indeces = itr.next();
                printer.println(indeces.getGroupID() + "\t" + 
                                indeces.getPeptides().size() + "\t" + 
                                indeces.getGlycosites().size() + "\t" + 
                                indeces.getUniquePeptides().size() + "\t" + 
                                indeces.getUniqueGlycosites().size() + "\t" + 
                                indeces.getSpecificity());
            }           
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GroupGlycIndecesPrinter.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            printer.close();
        }
    }
        
    
    
}
