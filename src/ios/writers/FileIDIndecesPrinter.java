/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package ios.writers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import xglycscan.XGSFileIDIndeces;

/**
 *
 * @author paiyeta1
 */
public class FileIDIndecesPrinter {

    public void print(ArrayList<XGSFileIDIndeces> iDIndeces, String output) {
        PrintWriter printer = null;
        
        try {
            //String output = outputDir + File.separator + new Date().getTime() + ".identification.indeces";
            printer = new PrintWriter(new File(output));
            // print header
            /*
             * File(Sample) name
             * Identified peptides
             * Identified glycosites
             * Unique peptides
             * Unique glycosites
             * Specificity
             * 
             */
            printer.println("File(Sample) name" + "\t" + 
                            "Identified peptides" + "\t" + 
                            "Identified glycosites" + "\t" + 
                            "Unique peptides" + "\t" + 
                            "Unique glycosites"  + "\t" + 
                            "Specificity");
            // print respective file identification indeces
            Iterator<XGSFileIDIndeces> itr = iDIndeces.iterator();
            while(itr.hasNext()){
                XGSFileIDIndeces indeces = itr.next();
                printer.println(new File(indeces.getFileName()).getName() + "\t" + 
                                indeces.getNumberOfIDPeptides() + "\t" + 
                                indeces.getNumberOfIDGlycosites() + "\t" + 
                                indeces.getNumberOfUniqIDPeptides() + "\t" + 
                                indeces.getNumberOfUniqIDGlycosites() + "\t" + 
                                indeces.getSpecificity());
            }           
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(FileIDIndecesPrinter.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            printer.close();
        }
        
        
    }
    
}
