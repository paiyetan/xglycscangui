/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package ios.writers;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import xglycscan.enums.MatrixType;
import xglycscan.quan.analysis.QuanCountFileCumRunEffectObject;
import xglycscan.quan.analysis.QuanCountUniqGlycNCoefObject;
import xglycscan.quan.analysis.QuanSquareMatrix;

/**
 *
 * @author paiyeta1
 */
public class AnalysisResultPrinter {
    

    public void printIdentificationCoef(HashMap<String, QuanCountUniqGlycNCoefObject> file2GlycNCoefMap, String output) {
        
        try {
            PrintWriter printer = new PrintWriter(output);
            // print table header FileName\tUniqueFileSpecificGlycosites\tCoefficient
            printer.println("FileName\tUniqueFileSpecificGlycosites\tCoefficient");
            // print table data
            Set<String> ids = file2GlycNCoefMap.keySet();
            Iterator<String> itr = ids.iterator();
            while(itr.hasNext()){
                String id = itr.next();
                QuanCountUniqGlycNCoefObject qCUGlycNCoefObj = file2GlycNCoefMap.get(id);
                int uniqGlycCount = qCUGlycNCoefObj.getUniqueGlycsCount(); 
                double coef = qCUGlycNCoefObj.getCoefficient(); 
                printer.println(id + "\t" + uniqGlycCount + "\t" + coef);
            }
            printer.close();         
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AnalysisResultPrinter.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }

    public void printSquareMatrix(QuanSquareMatrix squareMatrix, MatrixType matrixType, String output) {
        
        String[] Ids = squareMatrix.getIds();
        
        try {
            // print header (colnames)
            PrintWriter printer = new PrintWriter(output);
            printer.print("\t");
            for(int i = 0; i < Ids.length; i++){
                if(i < (Ids.length - 1)){
                    printer.print(Ids[i] + "\t");
                } else {
                    printer.print(Ids[i] + "\n");
                }
            }
            // print rownames + matrix data 
            switch(matrixType){
                
                case INTEGER:
                    int[][] matrix = squareMatrix.getCountMatrix();
                    for(int i = 0; i < Ids.length; i++){
                        printer.print(Ids[i] + "\t"); // print rownames
                        for(int j = 0; j < Ids.length; j++){
                           if(j < (Ids.length - 1)){
                                printer.print(matrix[i][j] + "\t");
                           } else {
                                printer.print(matrix[i][j] + "\n");
                           }
                        }
                    }
                    break;
                case DOUBLE:
                    double[][] doubleMatrix = squareMatrix.getPercentMatrix();
                    for(int i = 0; i < Ids.length; i++){
                        printer.print(Ids[i] + "\t"); // print rownames
                        for(int j = 0; j < Ids.length; j++){
                           if(j < (Ids.length - 1)){
                                printer.print(doubleMatrix[i][j] + "\t");
                           } else {
                                printer.print(doubleMatrix[i][j] + "\n");
                           }
                        }
                    }
                    break; 
            }
            printer.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AnalysisResultPrinter.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    public void printGroupCummulativeFileEffects(ArrayList<QuanCountFileCumRunEffectObject> qcFCREObjs, 
                                                  String output) {
        try {
            PrintWriter printer = new PrintWriter(output);
            // print header 
            // FileID|pririority|uniqGlycCount|cummulativeCount|deltaCount;
            printer.println("FileID\tPririority\tUnique_Glycosites\tCummulative_Glycosites\tDelta_Count");
            for(int i = 0; i < qcFCREObjs.size(); i++){
                QuanCountFileCumRunEffectObject qcFCREObj = qcFCREObjs.get(i);
                printer.println(qcFCREObj.getId() + "\t" +
                                qcFCREObj.getPririority() + "\t" +
                                qcFCREObj.getUniqGlycCount() + "\t" +
                                qcFCREObj.getCummulativeCount() + "\t" +
                                qcFCREObj.getDeltaCount());
            }
            
            printer.close();
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AnalysisResultPrinter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    
}
