/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */


package ios.readers;


import db.Database;
import db.fastadb.FastaDb;
import db.fastadb.FastaDbEntry;
import java.io.*;
import java.util.ArrayList;
import xglycscan.enums.SearchDatabaseType;



/**
 * Loads a <code>FASTA</code> database from file into an array of <code>FastaDbEntry</code> objects.  Here,
 * a <code>FASTA</code> database is a text file in which each entry is delineated by the <code>'&gt;'</code>
 * character, immediately proceeded by a unique <code>IPI</code> identifier.  The amino acid sequence of the
 * entry follows on the next line, and continues until the start of the next entry.
 *
 * (c) Institute for Systems Biology
 *
 * @author	Paul Loriaux
 * @version $Id$
 * @since	1.0
 *
 * file		$RCSFile$
 * created	Thursday, October 07, 2004
 * modified	$Date$
 */


public class FastaDbReader extends Object{

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
    *																										*
    * 		Attributes																						*
    *																										*
    \* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /**
        * The <code>FASTA</code> file to be loaded by this <code>FastaDbReader</code>.
        */
    File fasta;

    /**
        * The number of entries in the <code>FASTA</code> file read by this <code>FastaDbReader</code>.
        */
    int numEntries = 1;
    boolean reprint = false;


    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
    *																										*
    * 		Constructor																						*
    *																										*
    \* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    public FastaDbReader(String fastafile, SearchDatabaseType dbtype, boolean reprint) throws FileNotFoundException{
        this(fastafile,dbtype);
        this.reprint = reprint;
    }

    /**
     * Creates a new <code>FastaDbReader</code> to read the contents of the <code>FASTA</code> file at the
     * given location.
     *
     * @param fastafile the name and the location of the <code>FASTA</code> file to be read by this <code>
     * FastaDbReader</code>.
     *
     * @throws FileNotFoundException if no file exists at the given location.
     */
    public FastaDbReader(String fastafile,SearchDatabaseType dbtype) throws FileNotFoundException{
        // Create a new Java Object
        super();

        // Create a File object for the Fasta file
        fasta = new File(fastafile);

        // How many entries are there?
        String line;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fasta));
            while((line = reader.readLine()) != null) if(isHeader(line,dbtype) != 0) numEntries++;
        } catch( Exception E ) {
            System.err.println( "An Exception occurred while counting the number of entries in the " +
                    "Fasta database file: " + E.getMessage() );
        }
    }


    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
    *																										*
    * 		Methods																							*
    *																										*
    \* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     * Returns <code>0</code> if the given <code>String</code> does not resemble a header line in a <code>
     * FASTA</code> file database.  Header lines are identified by their leading '&gt;' characters,
     * immediately followed by an eight digit IPI number.  If the given <code>String</code> <i>does</i>
     * resembe a header line, the non-zero value returned by this method will be the IPI number parsed from
     * the header.
     *
     * @param line the <code>String</code> to be examined.
     *
     * @return <code>0</code> if the given <code>String</code> does not resemble a header line in a <code>
     * FASTA</code> file database.  If the given <code>String</code> <i>does</i> resembe a header line, the
     * non-zero value returned by this method will be the IPI number parsed from the header.
     *
     * @throws Exception if there is an error parsing an <code>IPI</code> number from a header line.
     */

    private int isHeader( String line, SearchDatabaseType dbtype ) throws Exception{
        int ipi = 0;
        // Does the line begin with a '>' character?
        switch(dbtype){
            case IPIv387:
                if(line.startsWith(">")){
                    // Does the line continue with an eight digit IPI number?
                    line = line.substring( 1 );
                    if(line.startsWith("IPI")){
                        // Parse out the IPI number
                        // line = line.substring(0, 11);
                        line = line.substring(4, 15);
                        ipi = FastaDb.ipiToInt(line,dbtype);
                    }
                }
                break;
            default: //REFSEQ
                if(line.startsWith(">")){
                    // Does the line continue with an eight digit IPI number?
                    line = line.substring( 1 );
                    if(line.startsWith("gi")){
                        // Parse out the REFSEQ number
                        ipi = Integer.parseInt(line.split("\\|")[1]);
                        //ipi = FastaDb.ipiToInt(line);
                    }
                }
        }
        return ipi;
    } /* end isHeadeturn ipi; r( String ) */


    /**
     * Returns the set of <code>FastaDbEntry</code> objects whose sequence wholey contains the sequence
     * passed to this method.  NOTE: owing to the size of most Fasta database files, this method can only
     * operate by reading through the entire file on each call.  In otherwords, the method can be slow.
     * Optimization is possible by detecting the size of the file and deciding then whether to simply load
     * the whole thing into memory, but that seems unnecessary at present.
     *
     * @param querySeq the subsequence which is wholey contained by the sequences of all <code>FastaDbEntry
     * </code> objects returned by this method.
     *
     * @param out a <code>PrintStream</code> for streaming status info.
     *
     * @return the set of all <code>FastaDbEntry</code> objects whose sequence wholey contains the sequence
     * passed to this method.
     *
     * @throws IOException if there is any problem reading the Fasta database file.
     */
    public FastaDbEntry[] findEntriesWithSequence(String querySeq, SearchDatabaseType dbtype, PrintStream out) throws IOException{
        out.print( "Searching for sequence: " + querySeq );

        // The growing list of FastaDbEntry objects which contain the query sequence
        ArrayList entries = new ArrayList();

        // Create a BufferedReader to read the FASTA file
        BufferedReader reader = new BufferedReader( new FileReader(fasta) );

        // The current sequence being loaded from the file and the IPI of the entry
        int ipi = 0;
        int numHeaders = 0;
        int appends = 0;
        StringBuffer seq = new StringBuffer();
        String header = "";

        String line = reader.readLine();
        while( line != null ){
            // See if the next line is a header, and if so, get the IPI
            try{
                int potential_ipi = isHeader( new String(line), dbtype );

                // Nope, not a header
                if( potential_ipi == 0 ){
                    // Make sure we have a valid IPI (i.e. make sure the file began with a header)
                    if( ipi == 0 ) throw new IOException( "Fasta files must begin with an entry header line." );

                    // Trim the line and append it to the current sequence

                    seq.append( line.trim() );
                }
                // It's a header.  See if the sequence of the previous entry contains the query sequence.
                // If so, create a FastaDbEntry object and add it to the list of entries to be returned.
                else{
                    numHeaders++;
                    if( numHeaders > (int)(numEntries/20)){
                        numHeaders = 0;
                        appends++;
                        if(appends % 5 == 0) out.print(","); else out.print(".");
                    }

                    if(ipi != 0){
                        String entrySeq = seq.toString();
                        seq = new StringBuffer();
                        if( contains(entrySeq, querySeq) ) entries.add( new FastaDbEntry(ipi, header, entrySeq,dbtype) );
                        if( reprint ){
                            System.out.println( entrySeq );
                            System.out.println( line );
                        }
                    }
                    ipi = potential_ipi;
                    header = line;
                }

            } catch( Exception E ) {
                throw new IOException( "Could not parse IPI from line: \n" + line + "\n" + E.getMessage() );
            }

            // Read the next line
            line = reader.readLine();
        }

        // Create the last entry, if needbe
        String entrySeq = seq.toString();
        seq = new StringBuffer();
        if( contains(entrySeq, querySeq) ) entries.add( new FastaDbEntry(ipi, header, entrySeq, dbtype) );
        if( reprint ){
            System.out.println( entrySeq );
            System.out.println( line );
        }

        // Create and return the array of FastaDbEntry objects
        FastaDbEntry[] allEntries = new FastaDbEntry[entries.size()];
        entries.toArray( allEntries );

        for( int i = 0; i < allEntries.length; i++ ){
            out.print( FastaDb.ipiToString(allEntries[i].getIpi(),dbtype) );
            if( i != allEntries.length-1 ) out.print( ", " );
        }
        out.println( "\n" );

        return allEntries;

    } /* end findEntriesWithSequence( String ) */


    /**
     * Returns the set of <code>FastaDbEntry</code> objects whose headers contain the given <code>String
     * </code>.
     *
     * @param query the <code>String</code> that is to be in the header of each <code>FastaDbEntry
     * </code> object returned.
     *
     * @return the set of <code>FastaDbEntry</code> objects whose headers contain the given <code>String
     * </code>.
     *
     * @throws IOException if there is any problem reading the Fasta database file.
     */
    int linesLoaded = 0;
    public FastaDbEntry[] findEntriesWithHeaderContaining(String query, SearchDatabaseType dbtype) throws IOException{
        //if( query.trim().equals("") ) return new FastaDbEntry[] {};
        // The growing list of FastaDbEntry objects whose headers contain the query
        ArrayList entries = new ArrayList();

        // Create a BufferedReader to read the FASTA file
        BufferedReader reader = new BufferedReader(new FileReader(fasta));

        // Tag headers that contain the query sequence
        boolean headerContainsQuery = false;

        StringBuffer seq = new StringBuffer();
        String line = reader.readLine();
        String header = "";
        int ipi = 0;
        int linesRead = 0;
        while(line != null){
            // See if the next line is a header, and if so, get the IPI
            linesRead++;
            System.out.print(linesRead + ",");
            try{
                int potential_ipi = isHeader(line, dbtype);

                // Nope, not a header
                if(potential_ipi == 0){
                    // Make sure we have a valid IPI (i.e. make sure the file began with a header)
                    if(ipi == 0) throw new IOException( "Fasta files must begin with an entry header line." );

                    // Trim the line and append it to the current sequence
                    if(headerContainsQuery) seq.append(line.trim());
                }

                // It's a header.  See if the prevoius header contained the query sequence.  If so, create a
                // FastaDbEntry object and add it to the list of entries to be returned.
                else
                {
                    // Did the previous header contain the query sequence?
                    if( headerContainsQuery )
                    {
                        // Create a FastaDbEntry and add it to the list
                        String entrySeq = seq.toString();
                        seq = new StringBuffer();
                        entries.add( new FastaDbEntry(ipi, header, entrySeq, dbtype) );
                        headerContainsQuery = false;
                    }

                    ipi = potential_ipi;
                    header = line;

                    // Does this header contain the query sequence?
                    if( contains(line, query) ) headerContainsQuery = true;
                }

            } catch( Exception E ) {
                    throw new IOException( "Could not parse IPI from line: \n" + line + 
                            "\n" + ipi + "\n" + query + "\n" + headerContainsQuery + "\n" + E.getMessage() );
            }

            // Read the next line
            line = reader.readLine();
        }

        // Create the last entry, if needbe
        if( headerContainsQuery )
        {
            // Create a FastaDbEntry and add it to the list
            String entrySeq = seq.toString();
            seq = new StringBuffer();
            entries.add( new FastaDbEntry(ipi, header, entrySeq, dbtype) );
            headerContainsQuery = false;
        }

        // Create and return the array of FastaDbEntry objects
        FastaDbEntry[] allEntries = new FastaDbEntry[entries.size()];
        entries.toArray( allEntries );

        return allEntries;

    } /* end findEntriesWithHeaderContaining( String ) */


   /**
    * Loads the <code>FASTA</code> file with the given pathname.  If necessary, this method can be written
    * to work on a different thread.
    *
    * @param fastafile the pathname of the <code>FASTA</code> file to be loaded by this <code>FastaDbReader
    * </code>.
    *
    * @throws IOException if an <code>IOException</code> occurs during the load.
    */
 
    public static FastaDb load(Database db, SearchDatabaseType dbtype) throws IOException{
        // Create the FastaDb object
        FastaDbEntry[] entries = db.getDBAsFastaDBEntries(dbtype);
        FastaDb fastaDb = new FastaDb(entries);
        return fastaDb;

    } /* end load( String ) */


   /**
    * Returns <code>true</code> if <code>String</code> <i>s</i> contains <code>String</code> <i>sub</i>.
    *
    * @return <code>true</code> if <code>String</code> <i>s</i> contains <code>String</code> <i>sub</i>.
    */

    public static boolean contains(String s, String sub){
        if( sub.length() < 1 ) return true;

        char[] c = s.toCharArray();
        char[] cub = sub.toCharArray();

        int i = 0;
        int test_region = c.length - cub.length;
        while( i <= test_region ){
            int j = 0;
            while( j < cub.length  &&  c[i+j] == cub[j] ) j++;
            if( j == cub.length ) return true;
            i++;
        }
        return false;
    }




} /* end class FastaDbReader */