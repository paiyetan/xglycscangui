/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package ios.readers;

import db.searches.psmPeptide.PSMPeptide;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import uk.ac.ebi.jmzidml.MzIdentMLElement;
import uk.ac.ebi.jmzidml.model.mzidml.*;
import uk.ac.ebi.jmzidml.xml.io.MzIdentMLUnmarshaller;
import xglycscan.XGSMZIDPSMFDRCalculator;
import xglycscan.XGSMZIDPSMPeptideExtractor;
import xglycscan.enums.EvaluationValueType;

/**
 *
 * @author paiyeta1
 */
public class MzIdentMLFileReader {
    
    public ArrayList<PSMPeptide> read(String mzidFile, 
                                            EvaluationValueType evalueType, 
                                                boolean computeFDR, 
                                                    double fDRFilter,
                                                        String outTableDir,
                                                            boolean useTopRanked) throws FileNotFoundException{       
        ArrayList<PSMPeptide> psmPeps = new ArrayList<PSMPeptide>();
        //getMZIdentMLObject of file
        MzIdentMLUnmarshaller unmarshaller = new MzIdentMLUnmarshaller(new File(mzidFile));
        // map PeptideID to Peptide object
        HashMap<String,Peptide> pepID2PeptideMap = new HashMap<String,Peptide>();
        Iterator<Peptide> iterPeptide = unmarshaller.unmarshalCollectionFromXpath(MzIdentMLElement.Peptide);
        System.out.println("  Mapping MzIdentMLElement.Peptide peptideID to Peptide Object...");
        while(iterPeptide.hasNext()){
            Peptide peptide = iterPeptide.next();
            pepID2PeptideMap.put(peptide.getId(), peptide);
        }
        // map peptideIDs to a LinkedList of PeptideEvidence(s)
        HashMap<String, LinkedList<PeptideEvidence>> pepID2EvidencesMap = new HashMap<String, LinkedList<PeptideEvidence>>();
        Iterator<PeptideEvidence> iterPeptideEvi = unmarshaller.unmarshalCollectionFromXpath(MzIdentMLElement.PeptideEvidence);
        System.out.println("  Mapping MzIdentMLElement.Peptide peptideID to PeptideEvidences LinkedList Object...");
        while(iterPeptideEvi.hasNext()){
            PeptideEvidence pepEvidence = iterPeptideEvi.next();
            String peptideID = pepEvidence.getPeptideRef();
            if(pepID2EvidencesMap.containsKey(peptideID)){
                LinkedList<PeptideEvidence> pepEvidences = pepID2EvidencesMap.remove(peptideID);
                pepEvidences.add(pepEvidence);
                pepID2EvidencesMap.put(peptideID, pepEvidences);
            } else {
                LinkedList<PeptideEvidence> pepEvidences = new LinkedList<PeptideEvidence>();
                pepEvidences.add(pepEvidence);
                pepID2EvidencesMap.put(peptideID, pepEvidences);
            }
        }
        
        AnalysisData ad = unmarshaller.unmarshal(DataCollection.class).getAnalysisData();
        List<SpectrumIdentificationList> sil = ad.getSpectrumIdentificationList();
        SpectrumIdentificationList sidentList = sil.get(0);
        
        // Get top-ranked spectrumIdentificationItems 
        //System.out.println("  Getting top-ranked spectrumIdentificationItems...");
        System.out.println("  Getting spectrumIdentificationItems...");
        LinkedList<SpectrumIdentificationItem> rankedSIItems = new LinkedList<SpectrumIdentificationItem>();
        HashMap<String,String> siiId2sirIdMap = new HashMap<String,String>();
        if(useTopRanked){
            //LinkedList<SpectrumIdentificationItem> topRankedSIItems = new LinkedList<SpectrumIdentificationItem>();
            for(SpectrumIdentificationResult specIdR : sidentList.getSpectrumIdentificationResult()){
                List<SpectrumIdentificationItem> siitems = specIdR.getSpectrumIdentificationItem();
                SpectrumIdentificationItem sii = getTopRankedSpectrumIdentificationItem(siitems);
                rankedSIItems.add(sii);  
                // map spectrumIdentificationItem id to spectrumIdentificationResult id
                String sirId = specIdR.getSpectrumID(); // not id but spectrumID
                String siiId = sii.getId();
                siiId2sirIdMap.put(siiId, sirId);
            } 
        }else {
            //LinkedList<SpectrumIdentificationItem> topRankedSIItems = new LinkedList<SpectrumIdentificationItem>();
            for(SpectrumIdentificationResult specIdR : sidentList.getSpectrumIdentificationResult()){
                List<SpectrumIdentificationItem> siitems = specIdR.getSpectrumIdentificationItem();
                for(SpectrumIdentificationItem sii : siitems){
                    rankedSIItems.add(sii);
                    // map spectrumIdentificationItem id to spectrumIdentificationResult id
                    String sirId = specIdR.getSpectrumID(); // not id but spectrumID
                    String siiId = sii.getId();
                    siiId2sirIdMap.put(siiId, sirId);
                }
            }            
        }
        //System.out.println("   " + topRankedSIItems.size() + " top-ranked SpectrumIdentificationItems found...");                
        System.out.println("   " + rankedSIItems.size() + " ranked SpectrumIdentificationItems found...");           
        // Get (+/- FDR Filtered) top-ranked spectrumIdentificationItems MzIdentMLElement.Peptide Objects 
        String specFile = new File(mzidFile).getName();    
        if(computeFDR){            
            System.out.println("  Estimating p-values and Multiple testing error correction False Discovery Rates (FDR)..."); 
            XGSMZIDPSMFDRCalculator fDRCalculator = new XGSMZIDPSMFDRCalculator(specFile, 
                                                                                fDRFilter, 
                                                                                rankedSIItems, 
                                                                                pepID2PeptideMap, 
                                                                                pepID2EvidencesMap, 
                                                                                evalueType,
                                                                                outTableDir,
                                                                                siiId2sirIdMap);
            psmPeps = fDRCalculator.getPsmPeps();              
        } else {            
            for(SpectrumIdentificationItem sii: rankedSIItems){
                if(allEvidenceIsDecoy(pepID2PeptideMap.get(sii.getPeptideRef()).getId(), pepID2EvidencesMap)==false){
                    XGSMZIDPSMPeptideExtractor psmPepExtractor = new XGSMZIDPSMPeptideExtractor();
                    PSMPeptide psmPep = psmPepExtractor.getPeptide(specFile, 
                                                                    sii, 
                                                                    pepID2PeptideMap, 
                                                                    pepID2EvidencesMap, 
                                                                    evalueType,
                                                                    siiId2sirIdMap);
                    psmPeps.add(psmPep);
                }                
            }           
        }
        return psmPeps;      
    }
    
    private SpectrumIdentificationItem getTopRankedSpectrumIdentificationItem(List<SpectrumIdentificationItem> siitems) {
        //throw new UnsupportedOperationException("Not yet implemented");
        SpectrumIdentificationItem topRankedSII = siitems.get(0);
        for(int i = 0; i < siitems.size(); i++){
            if(siitems.get(i).getRank() < topRankedSII.getRank()){
                topRankedSII = siitems.get(i);
            }
        }       
        return topRankedSII;
    }

    private boolean allEvidenceIsDecoy(String peptideID, HashMap<String, LinkedList<PeptideEvidence>> pepID2EvidencesMap) {
        //throw new UnsupportedOperationException("Not yet implemented");
        boolean allDecoys  = true;
        
        LinkedList<PeptideEvidence> pepEvidences = pepID2EvidencesMap.get(peptideID);
        Iterator<PeptideEvidence> itr = pepEvidences.iterator();
        while(itr.hasNext()){
            PeptideEvidence pepEvidence = itr.next();
            if(pepEvidence.isIsDecoy()==false){
                allDecoys = false;
                break;
            }
        }
        return allDecoys;
    }
}
