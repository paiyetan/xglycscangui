/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package xglycscan.groupindecesacc;

import java.util.*;

/**
 *
 * @author paiyeta1
 */
public class GlycMatrixExtractor {
    
    private HashMap<String,GroupIndeces> iD2IndecesMap; //map groupID2GroupIndeces object
    private HashMap<String,LinkedList<String>> glyc2IDMap; // glycosite mapped to identifier (group or file) within which it is found 
    private GlycCountMatrix matrix;
    
    public GlycMatrixExtractor(ArrayList<GroupIndeces> groupIndeces){
        
        extractID2Indeces(groupIndeces); //map groupID2GroupIndeces object
        System.out.println("  Mapping glycosite(s) to files within which they were identified...");
        generateGlycMap(groupIndeces);
        System.out.println("  Extracting matrix...");
        this.matrix = new GlycCountMatrix(iD2IndecesMap, glyc2IDMap);       
    }
    
    private void extractID2Indeces(ArrayList<GroupIndeces> groupIndeces) {
        iD2IndecesMap = new HashMap<String,GroupIndeces>();
        for(GroupIndeces groupInd: groupIndeces){
            iD2IndecesMap.put(groupInd.getGroupID(), groupInd);
        }       
    }

    private void generateGlycMap(ArrayList<GroupIndeces> groupIndeces) {
        glyc2IDMap = new HashMap<String, LinkedList<String>>();
        // iterate through all glycosites derived,
        // glyc2IDMap derived glyc(s) to GroupIndecesID within which it is found 
        Iterator<GroupIndeces> itr = groupIndeces.iterator();
        
        while(itr.hasNext()){
            GroupIndeces groupIndecesObj = itr.next(); 
            ArrayList<String> fileGlycSeqs =  groupIndecesObj.getUniqueGlycosites();
            
            Iterator<String> itr2 = fileGlycSeqs.iterator();
            while(itr2.hasNext()){
                String glycSeq = itr2.next();
                if(glyc2IDMap.containsKey(glycSeq) == false){
                    LinkedList<String> mappedIDs = new LinkedList<String>();
                    mappedIDs.add(groupIndecesObj.getGroupID());
                    glyc2IDMap.put(glycSeq,mappedIDs);
                   
                } else {
                    LinkedList<String> mappedIDs = glyc2IDMap.remove(glycSeq);
                    mappedIDs.add(groupIndecesObj.getGroupID());
                    glyc2IDMap.put(glycSeq,mappedIDs); 
                }
            }
         }
    }

    public HashMap<String, LinkedList<String>> getGlyc2IDMap() {
        return glyc2IDMap;
    }

    public GlycCountMatrix getGlycCountMatrix() {
        return matrix;
    }    
    
}
