/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package xglycscan.groupindecesacc;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Set;
import xglycscan.XGSFileIDIndeces;

/**
 *
 * @author paiyeta1
 */
public class GroupIndecesExtractor {
    
    public ArrayList<GroupIndeces> extractGroupIndeces(HashMap<String, LinkedList<String>> pheno2FilesMap, 
                                                           ArrayList<XGSFileIDIndeces> iDIndeces,
                                                               String groupOutTableDir) throws FileNotFoundException {
        ArrayList<GroupIndeces> groupIndeces = new ArrayList<GroupIndeces>();
        HashMap<String,XGSFileIDIndeces> file2FileIndecesMap = mapFile2FileIndeces(iDIndeces);
        //instantiateGroupNFileIndecesOutput(indecesOutputDir);
        PrintWriter printGrpNFileIndeces = new PrintWriter(groupOutTableDir + File.separator + "group.members.identification.indeces");
        //print header
        printGrpNFileIndeces.println("Group (Phenotype) name" + "\t" +
                                     "File(Sample) name" + "\t" + 
                                     "Identified peptides" + "\t" + 
                                     "Identified glycosites" + "\t" + 
                                     "Unique peptides" + "\t" + 
                                     "Unique glycosites"  + "\t" + 
                                     "Specificity");
        Set<String> phenos = pheno2FilesMap.keySet();
        for(String pheno : phenos){
            ArrayList<XGSFileIDIndeces> glycFileIndeces = new ArrayList<XGSFileIDIndeces>();
            LinkedList<String> mappedFiles = pheno2FilesMap.get(pheno);
            for(String mappedFile: mappedFiles){
                //GlycFileIndecesExtractor gFileExtractor = new GlycFileIndecesExtractor();
                //GlycFileIndeces fileIndeces = gFileExtractor.extractGlycFileIndeces(mappedFile, file2GlycFilePath.get(mappedFile));
                XGSFileIDIndeces fileIndeces = file2FileIndecesMap.get(mappedFile);
                // output file indeces...
                printGrpNFileIndeces.println(pheno + "\t" +
                                             new File(fileIndeces.getFileName()).getName() + "\t" + 
                                             fileIndeces.getNumberOfIDPeptides() + "\t" + 
                                             fileIndeces.getNumberOfIDGlycosites() + "\t" + 
                                             fileIndeces.getNumberOfUniqIDPeptides() + "\t" + 
                                             fileIndeces.getNumberOfUniqIDGlycosites() + "\t" + 
                                             fileIndeces.getSpecificity()
                                            );
                                                
                glycFileIndeces.add(fileIndeces);
            }
            GroupIndeces thisGroupIndeces = getGroupIndeces(pheno, glycFileIndeces);
            groupIndeces.add(thisGroupIndeces);
        }
        printGrpNFileIndeces.close();
        return groupIndeces;  
    }
    
    
    private GroupIndeces getGroupIndeces(String pheno, ArrayList<XGSFileIDIndeces> glycFileIndeces) {
        GroupIndeces groupInd;
        String groupID = pheno; //phenotype
        //int numberOfPeptides = 0;
        //int numberOfGlycosites = 0;
        ArrayList<String> peptides = new ArrayList<String>();
        ArrayList<String> glycosites = new ArrayList<String>();
        for(XGSFileIDIndeces fileIndeces: glycFileIndeces){
            ArrayList<String> filePeptides = fileIndeces.getiDPeps();
            for(String filePeptide: filePeptides){
                peptides.add(filePeptide);
            }
            ArrayList<String> fileGlycosites = fileIndeces.getiDGlycs();
            for(String fileGlycosite: fileGlycosites){
                glycosites.add(fileGlycosite);
            }
        }
        groupInd = new GroupIndeces(groupID, peptides, glycosites);        
        return groupInd;
    }
    
    private HashMap<String, XGSFileIDIndeces> mapFile2FileIndeces(ArrayList<XGSFileIDIndeces> iDIndeces) {
        //throw new UnsupportedOperationException("Not yet implemented");
        HashMap<String, XGSFileIDIndeces> file2FileIndecesMap = new HashMap<String, XGSFileIDIndeces>();
        for(XGSFileIDIndeces index: iDIndeces){
            String fileName = new File(index.getFileName()).getName();
            file2FileIndecesMap.put(fileName, index);
        }
        return file2FileIndecesMap;
    }

    
    
}
