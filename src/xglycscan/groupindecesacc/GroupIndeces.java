/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package xglycscan.groupindecesacc;

import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author paiyeta1
 */
public class GroupIndeces {
    
    private String groupID; //phenotype
    private ArrayList<String> peptides;
    private ArrayList<String> glycosites;
    
    private HashMap<String,Integer> glyc2Count;
    //private double specificity;

    public GroupIndeces(String groupID, ArrayList<String> peptides, ArrayList<String> glycosites) {
        this.groupID = groupID;
        this.peptides = peptides;
        this.glycosites = glycosites;
        setGlyc2CountMap();
    }
    
    public String getGroupID() {
        return groupID;
    }

    public ArrayList<String> getGlycosites() {
        return glycosites;
    }
   
    public ArrayList<String> getPeptides() {
        return peptides;
    }
    
    public ArrayList<String> getUniqueGlycosites() {
        ArrayList<String> uniqGlycs = new ArrayList<String>();
        for(String glyc : glycosites){
            if(uniqGlycs.contains(glyc)==false){
                uniqGlycs.add(glyc);
            }
        }       
        return uniqGlycs;
    }

    public ArrayList<String> getUniquePeptides() {
        ArrayList<String> uniqPeps = new ArrayList<String>();
        for(String pep : peptides){
            if(uniqPeps.contains(pep)==false){
                uniqPeps.add(pep);
            }
        }  
        return uniqPeps;
    }

    public HashMap<String, Integer> getGlyc2Count() {
        return glyc2Count;
    }
    
     private void setGlyc2CountMap() {
        glyc2Count = new HashMap<String,Integer>();
        //ArrayList<String> uniqG = getUniqueGlycosites();
        for(String glyc : glycosites){
            if(glyc2Count.containsKey(glyc)){
                int current_count = glyc2Count.remove(glyc); // current glycosite count
                current_count++; // update count
                glyc2Count.put(glyc, current_count); // reinsert updated value
            }else{
                glyc2Count.put(glyc, 1);
            }
        }
        
        
    }

    public double getSpecificity() {
        return ((double) glycosites.size()/(double) peptides.size());
    }
    
    
    
    
    
}
