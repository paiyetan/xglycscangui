/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package xglycscan.quan;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

/**
 *
 * @author paiyeta1
 */
public class QuanCountDataMatrix {
    
    
    private String[] glycosites;
    private String[] fileNames;
    private int[][] matrix;

    public QuanCountDataMatrix(HashMap<String, String> file2SampleMap, 
            HashMap<String, LinkedList<QuanCountFileObject>> glyc2IDMap) {
        
        //throw new UnsupportedOperationException("Not yet implemented");
        setGlycArray(glyc2IDMap);
        setSampleNames(file2SampleMap);
        
        matrix = new int[glycosites.length][fileNames.length];
        
        //initialize array
        for(int i = 0; i < glycosites.length; i++){
            
            for(int j = 0; j <fileNames.length; j ++){
                matrix[i][j] = 0;
            }
            
        }        
        populateMatrix(glyc2IDMap);
        
    }

    public QuanCountDataMatrix(String[] phenotypes, String[] glycositeSeqs, int[][] matrix) {
        //throw new UnsupportedOperationException("Not yet implemented");
        fileNames = phenotypes;
        glycosites = glycositeSeqs;
        this.matrix = matrix;
    }

    private void setGlycArray(HashMap<String, LinkedList<QuanCountFileObject>> glyc2IDMap) {
        
        Set<String> glycs = glyc2IDMap.keySet();
        glycosites = new String[glycs.size()];
        Iterator<String> itr = glycs.iterator();
        int index = 0;
        while(itr.hasNext()){
            String glyc = itr.next();
            glycosites[index] = glyc;
            index++;           
        }
    }

    private void setSampleNames(HashMap<String, String> file2SampleMap) {
        
        Set<String> mapKeys = file2SampleMap.keySet();
        fileNames = new String[mapKeys.size()];
        Iterator<String> itr = mapKeys.iterator();
        int index = 0;
        while(itr.hasNext()){
            String key = itr.next();
            fileNames[index] = key;
            index++;           
        }
    }

    private void populateMatrix(HashMap<String, LinkedList<QuanCountFileObject>> glyc2IDMap) {
        //throw new UnsupportedOperationException("Not yet implemented");
        //throw new UnsupportedOperationException("Not yet implemented");
        for(int i = 0; i < glycosites.length; i++){
            String glyc = glycosites[i];
            for(int j = 0; j < fileNames.length; j++){
                String fileName = fileNames[j]; // in the case of an iTRAQ4plex pheno object, this is cancatenated with the respective channel ID 
                
                int glycosite_count = 0;
                
                LinkedList<QuanCountFileObject> quanFileObjs = glyc2IDMap.get(glyc);
                Iterator<QuanCountFileObject> itr = quanFileObjs.iterator();
                while(itr.hasNext()){
                    QuanCountFileObject quanFileObj = itr.next();
                    if(quanFileObj.getFilename().equalsIgnoreCase(fileName)){
                        glycosite_count = quanFileObj.getGlycSeq2CountMap().get(glyc);
                        break;
                    }
                }
                //double channel_value
                matrix[i][j] = glycosite_count;
            }
        }
    }

    public String[] getFileNames() {
        return fileNames;
    }
    
    public String[] getPhenotypes() {
        return fileNames;
    }

    public String[] getGlycosites() {
        return glycosites;
    }

    public int[][] getMatrix() {
        return matrix;
    }
    
    
    
    
}
