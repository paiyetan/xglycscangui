/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package xglycscan.quan;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import xglycscan.quan.analysis.QuanCountFileCumRunEffectObject;
import xglycscan.quan.analysis.QuanCountPriorityQueue;

/**
 *
 * @author paiyeta1
 */
public class QuanCountGroupObject {
    
    private String groupName;
    private ArrayList<String> uniqueGlycSeqs;
    private HashMap<String, Integer> glycSeq2CountMap;
    private ArrayList<QuanCountFileObject> qfileObjs;
    
    public QuanCountGroupObject(String name,  ArrayList<QuanCountFileObject> quanCountFileObjs){
        groupName = name;
        qfileObjs = quanCountFileObjs;
        setUniqGlycosites();
        setGlycSeq2CountMap();
    }

    private void setUniqGlycosites() {
        //throw new UnsupportedOperationException("Not yet implemented");
        uniqueGlycSeqs = new ArrayList<String>();
        for(int i = 0; i < qfileObjs.size(); i++){
            ArrayList<String> fileUniqGlycs = qfileObjs.get(i).getUniqueGlycSeqs();
            Iterator<String> itr = fileUniqGlycs.iterator();
            while(itr.hasNext()){
                String glyc = itr.next();
                if(uniqueGlycSeqs.contains(glyc)){
                    //do nothing
                } else {
                    uniqueGlycSeqs.add(glyc);
                }
            }               
        }
    }

    private void setGlycSeq2CountMap() {
        //throw new UnsupportedOperationException("Not yet implemented");
        glycSeq2CountMap = new HashMap<String, Integer>();
        for(int i = 0; i < qfileObjs.size(); i++){
            HashMap<String, Integer> fileGlyc2CountMap = qfileObjs.get(i).getGlycSeq2CountMap();
            Set<String> glycs = fileGlyc2CountMap.keySet();
            Iterator<String> itr = glycs.iterator();
            while(itr.hasNext()){
                String glyc = itr.next();
                if(glycSeq2CountMap.containsKey(glyc)){
                    //add file's count to global count to update actual count.
                    int updatedCount = glycSeq2CountMap.get(glyc) + fileGlyc2CountMap.get(glyc);
                    glycSeq2CountMap.put(glyc, updatedCount);
                } else {
                    glycSeq2CountMap.put(glyc, fileGlyc2CountMap.get(glyc));
                }
            }               
        }
    }

    public HashMap<String, Integer> getGlycSeq2CountMap() {
        return glycSeq2CountMap;
    }

    public String getGroupName() {
        return groupName;
    }

    public ArrayList<QuanCountFileObject> getQfileObjs() {
        return qfileObjs;
    }

    public ArrayList<String> getUniqueGlycSeqs() {
        return uniqueGlycSeqs;
    }
    
    public HashMap<String, Integer> calcWithinFileObjUniqGlycs(){
        HashMap<String, Integer> file2UniqGlycCount = new HashMap<String, Integer>();
        //iteratively compare each unique element(peptide) of each file with those in other files
        for(int i = 0; i < qfileObjs.size(); i++){
            QuanCountFileObject qfileObj = qfileObjs.get(i);
            String refName = qfileObj.getFilename();
            ArrayList<String> uniqGlycs = qfileObj.getUniqueGlycSeqs();
            int fileUniqGlycCount = 0;
            Iterator<String> itr = uniqGlycs.iterator();
            while(itr.hasNext()){
                String fileUniqGlyc = itr.next();
                if(isUniqueToFile(fileUniqGlyc, i)){
                    fileUniqGlycCount++;
                }
            }         
            file2UniqGlycCount.put(refName, fileUniqGlycCount);
            System.out.println(fileUniqGlycCount + " glycosite(s) found unique to " + refName + 
                    " among " + groupName);             
        }		       
        return file2UniqGlycCount;
    }

    private boolean isUniqueToFile(String fileUniqGlyc, int glycFileIndex) {
        boolean uniqToFile = true;
        for(int i = 0; i < qfileObjs.size(); i++){
            if(i != glycFileIndex){
                QuanCountFileObject qfileObj = qfileObjs.get(i);
                ArrayList<String> uniqGlycs = qfileObj.getUniqueGlycSeqs();
                Iterator<String> itr = uniqGlycs.iterator();
                while(itr.hasNext()){
                    String glyc = itr.next();
                    if(glyc.equalsIgnoreCase(fileUniqGlyc)){
                        uniqToFile = false;
                        break;
                    }
                }
            }
        }
        return uniqToFile;        
    }
    
    public int intersect(QuanCountGroupObject qCGObj){
        int intersect = 0;
        ArrayList<String> uniqGlycs = qCGObj.getUniqueGlycSeqs();
        Iterator<String> itr = uniqGlycs.iterator();
        while(itr.hasNext()){
            String glyc = itr.next();
            if(uniqueGlycSeqs.contains(glyc)){
                intersect++;               
            }
        }       
        return intersect;
    }
    
    public int diff(QuanCountGroupObject qCGObj){ //NB: this returns the number of identified glycosites
                                                  //in "this" but not in the qCObj passed as parameter                                  
        int diff = 0;      
        ArrayList<String> uniqGlycs = qCGObj.getUniqueGlycSeqs();
        Iterator<String> itr = uniqueGlycSeqs.iterator();
        while(itr.hasNext()){
            String glyc = itr.next();
            if(uniqGlycs.contains(glyc) == false){
                diff++;               
            }
        }     
        return diff;
    }
    
    public int union(QuanCountGroupObject qCGObj){ //NB: this returns the number of identified glycosites
        
        int union = uniqueGlycSeqs.size();
        ArrayList<String> uniqGlycs = qCGObj.getUniqueGlycSeqs();
        Iterator<String> itr = uniqGlycs.iterator();
        while(itr.hasNext()){
            String glyc = itr.next();
            if(uniqueGlycSeqs.contains(glyc) == false){
                union++;               
            }
        } 
        return union;
    }
    
    public double similarity(QuanCountGroupObject qCGObj){
        double similarity = (((double)intersect(qCGObj)/(double) union(qCGObj)) * 100);
        return similarity;
    }
    
    public ArrayList<QuanCountFileCumRunEffectObject> getCummEffectObjects(){
        ArrayList<QuanCountFileCumRunEffectObject> effectObjs = new ArrayList<QuanCountFileCumRunEffectObject>();
        QuanCountPriorityQueue priorityQueue = new QuanCountPriorityQueue(qfileObjs);
        int priorityIndex = 0;
        ArrayList<String> uniqCummGlycs = new ArrayList<String>();
        while(priorityQueue.isEmpty() == false){
            priorityIndex++;
            QuanCountFileObject qcFObj = priorityQueue.poll();
            /*
             *  String id; // usually file/Run name or id
                int pririority;
                int uniqGlycCount;
                int cummulativeCount;
                int deltaCount;
             */
            String id = qcFObj.getFilename();
            int pririority = priorityIndex;
            ArrayList<String> fObjUniqGlycs = qcFObj.getUniqueGlycSeqs();
            int uniqGlycCount = fObjUniqGlycs.size();
            //cummulate unique glycosite
            int precummulativeCount = uniqCummGlycs.size(); 
            Iterator<String> itr = fObjUniqGlycs.iterator();
            while(itr.hasNext()){
                String glyc = itr.next();
                if(uniqCummGlycs.contains(glyc) == false){
                    uniqCummGlycs.add(glyc);
                }
            }
            int cummulativeCount = uniqCummGlycs.size();
            int deltaCount = cummulativeCount - precummulativeCount;
            QuanCountFileCumRunEffectObject qcfCREObj = 
                    new QuanCountFileCumRunEffectObject(id, pririority, uniqGlycCount, 
                                                        cummulativeCount, deltaCount);
            effectObjs.add(qcfCREObj);
        }               
        return effectObjs;
    }
    
    
    
    
    
    
}
