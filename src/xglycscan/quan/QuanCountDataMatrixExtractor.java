/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package xglycscan.quan;

import java.util.*;

/**
 *
 * @author paiyeta1
 */
public class QuanCountDataMatrixExtractor {
    
    HashMap<String,String> file2SampleMap;
    private HashMap<String,LinkedList<QuanCountFileObject>> glyc2IDMap; // glycosite mapped to files within which it is found 
    private QuanCountDataMatrix matrix;
    
    
    public QuanCountDataMatrixExtractor(ArrayList<QuanCountFileObject> fileQuanObjs, 
                                                    HashMap<String,String> file2SampleMap){
        
        this.file2SampleMap = file2SampleMap;
        // generate glycosite to files within 
        System.out.println("  Mapping glycosite(s) to files within which they were identified...");
        this.glyc2IDMap = generateGlycMap(fileQuanObjs);
        // ExtractMatrix
        System.out.println("  Extracting matrix...");
        this.matrix = new QuanCountDataMatrix(file2SampleMap, glyc2IDMap);
        
        
    }

    private HashMap<String, LinkedList<QuanCountFileObject>> generateGlycMap(ArrayList<QuanCountFileObject> fileQuanObjs) {
        HashMap<String, LinkedList<QuanCountFileObject>> map = 
                new HashMap<String, LinkedList<QuanCountFileObject>>();
        // iterate through all glycosites derived,
        // map derived glyc(s) to QuanCountFileObjects within which it is found 
        Iterator<QuanCountFileObject> itr = fileQuanObjs.iterator();
        
        while(itr.hasNext()){
            QuanCountFileObject quanFileObj = itr.next(); 
            ArrayList<String> fileGlycSeqs =  quanFileObj.getUniqueGlycSeqs();
            
            Iterator<String> itr2 = fileGlycSeqs.iterator();
            while(itr2.hasNext()){
                String glycSeq = itr2.next();
                if(map.containsKey(glycSeq) == false){
                   
                    LinkedList<QuanCountFileObject> quanCountFileObjsOfGlyc = new LinkedList<QuanCountFileObject>();
                    quanCountFileObjsOfGlyc.add(quanFileObj);
                    map.put(glycSeq,quanCountFileObjsOfGlyc);
                   
                } else {
                    LinkedList<QuanCountFileObject> quanCountFileObjsOfGlyc = map.remove(glycSeq);
                    quanCountFileObjsOfGlyc.add(quanFileObj);
                    map.put(glycSeq,quanCountFileObjsOfGlyc); 
                }
            }
         }
               
         return map;       
    }

    public HashMap<String, LinkedList<QuanCountFileObject>> getGlyc2IDMap() {
        return glyc2IDMap;
    }

    public QuanCountDataMatrix getQuanCountDataMatrix() {
        return matrix;
    }
    
    public QuanCountDataMatrix getGroupsQuanCountDataMatrix(){
        String[] phenotypes = extractPhenotypes();
        String[] glycositeSeqs = extractGlycosites();
        int[][] matrix = extractGroupDataMatrix(glycositeSeqs, phenotypes);
        QuanCountDataMatrix groupMatrix = new QuanCountDataMatrix(phenotypes, glycositeSeqs, matrix);
        return groupMatrix;
    }

    private String[] extractPhenotypes() {
        String[] phenotypes = null;
        Set<String> filesMapped = file2SampleMap.keySet();
        ArrayList<String> phenotypeArrList = new ArrayList<String>();
        Iterator<String> itr = filesMapped.iterator();
        while(itr.hasNext()){
            String mappedFile =  itr.next();
            String phenotype = file2SampleMap.get(mappedFile);
            if(phenotypeArrList.contains(phenotype) == false){
                phenotypeArrList.add(phenotype);
            }
        }
        phenotypes = toArray(phenotypeArrList);
        return phenotypes;
    }
    private String[] toArray(ArrayList<String> phenotypeArrList) {
        //throw new UnsupportedOperationException("Not yet implemented");
        String[] arr = new String[phenotypeArrList.size()];
        for(int i = 0; i < arr.length; i++){
            arr[i] = phenotypeArrList.get(i);
        }
        return arr;
    }

    private String[] extractGlycosites() {
        
        String[] glycositeSeqs;
        Set<String> glycs = glyc2IDMap.keySet();
        glycositeSeqs = new String[glycs.size()];
        Iterator<String> itr = glycs.iterator();
        int index = 0;
        while(itr.hasNext()){
            String glyc = itr.next();
            glycositeSeqs[index] = glyc;
            index++;           
        }
        return glycositeSeqs;
    }

    private int[][] extractGroupDataMatrix(String[] glycositeSeqs, String[] phenotypes) {
        
        int[][] countMatrix = new int[glycositeSeqs.length][phenotypes.length];
        //map sampletype/phenotype to linkedlist of file members 
        HashMap<String,LinkedList<String>> pheno2FilesMap = mapPheno2Files(phenotypes);
        //initialize array
        for(int i = 0; i < glycositeSeqs.length; i++){
            for(int j = 0; j < phenotypes.length; j ++){
                countMatrix[i][j] = 0;
            }           
        }
        //populate matrix
        for(int i = 0; i < glycositeSeqs.length; i++){
            String glyc = glycositeSeqs[i];
            for(int j = 0; j < phenotypes.length; j ++){
                String pheno = phenotypes[j];
                int count = 0; //pheno spectra count
                //get members of phenotype
                LinkedList<String> phenoMappedFiles = pheno2FilesMap.get(pheno);
                LinkedList<QuanCountFileObject> glycMappedQuanObjs = glyc2IDMap.get(glyc);
                Iterator<QuanCountFileObject> itr = glycMappedQuanObjs.iterator();
                while(itr.hasNext()){
                    QuanCountFileObject qcFileObj = itr.next();
                    if(phenoMappedFiles.contains(qcFileObj.getFilename())){
                        count = count + qcFileObj.getGlycSeq2CountMap().get(glyc);
                    }
                }                    
                countMatrix[i][j] = count;
            }           
        }       
        return countMatrix;
    }

    private HashMap<String, LinkedList<String>> mapPheno2Files(String[] phenotypes) {
        HashMap<String,LinkedList<String>> pheno2FilesMap = new HashMap<String,LinkedList<String>>();
        Set<String> files = file2SampleMap.keySet();
        for(int i = 0; i < phenotypes.length; i++){
            String phenotype = phenotypes[i];
            Iterator<String> itr = files.iterator();
            while(itr.hasNext()){
                String file = itr.next();
                if(file2SampleMap.get(file).equalsIgnoreCase(phenotype)){
                    //check if a sample/phenotype to file(s) mapping is in the map
                    if(pheno2FilesMap.containsKey(phenotype)){
                        LinkedList<String> mappedFiles = pheno2FilesMap.remove(phenotype);
                        mappedFiles.add(file);
                        pheno2FilesMap.put(phenotype, mappedFiles);
                    } else {
                        LinkedList<String> mappedFiles = new LinkedList<String>();
                        mappedFiles.add(file);
                        pheno2FilesMap.put(phenotype, mappedFiles);
                    }
                }
            }
        }
        
        return pheno2FilesMap;
    }

    
    
    
    
    
}
