/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package xglycscan.quan;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author paiyeta1
 * @date 
 * The QuanCountFileObject holds the following attributes, the associated filename of object,
 * an ArrayList of uniquely identified glycosite peptide sequences in the file, and a map of
 * identified glycosites to count of glycosite.
 * 
 */
public class QuanCountFileObject {
    
    private String filename;
    private ArrayList<String> uniqueGlycSeqs; //typically the formatted sequence
    private HashMap<String, Integer> glycSeq2CountMap;

    public QuanCountFileObject(String filename, ArrayList<String> uniqueGlycSeqs, 
                                         HashMap<String, Integer> glycSeq2CountMap) {       
        this.filename = filename;
        this.uniqueGlycSeqs = uniqueGlycSeqs;
        this.glycSeq2CountMap = glycSeq2CountMap;        
    }

    public String getFilename() {
        return filename;
    }

    public ArrayList<String> getUniqueGlycSeqs() {
        return uniqueGlycSeqs;
    }

    public HashMap<String, Integer> getGlycSeq2CountMap() {
        return glycSeq2CountMap;
    }
    
    public int intersect(QuanCountFileObject qCFObj){
        int intersect = 0;
        ArrayList<String> uniqGlycs = qCFObj.getUniqueGlycSeqs();
        Iterator<String> itr = uniqGlycs.iterator();
        while(itr.hasNext()){
            String glyc = itr.next();
            if(this.uniqueGlycSeqs.contains(glyc)){
                intersect++;               
            }
        }       
        return intersect;
    }
    
    public int diff(QuanCountFileObject qCFObj){ //NB: this returns the number of identified glycosites
                                                  //in "this" but not in the qCObj passed as parameter                                  
        int diff = 0;      
        ArrayList<String> uniqGlycs = qCFObj.getUniqueGlycSeqs();
        Iterator<String> itr = this.uniqueGlycSeqs.iterator();
        while(itr.hasNext()){
            String glyc = itr.next();
            if(uniqGlycs.contains(glyc) == false){
                diff++;               
            }
        }     
        return diff;
    }
    
    public int union(QuanCountFileObject qCFObj){ 
        
        ArrayList<String> uniqGlycs = qCFObj.getUniqueGlycSeqs();
        
        int union = uniqueGlycSeqs.size();
        Iterator<String> itr = uniqGlycs.iterator();
        while(itr.hasNext()){
            String glyc = itr.next();
            if(uniqueGlycSeqs.contains(glyc) == false){
                union++;               
            }
        }     
        return union;
    }
    
    public double similarity(QuanCountFileObject qCFObj){
        double similarity = (((double)intersect(qCFObj)/(double) union(qCFObj)) * 100);
        return similarity;
    }
    
    
}
