/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package xglycscan.quan.analysis;

/**
 *
 * @author paiyeta1
 * 
 * QuanCountUniqGlycNCoefObject; Quan count unique [file or group] glycosites and specificity object
 */
public class QuanCountUniqGlycNCoefObject {
    
    private String id;
    private int uniqueGlycsCount;
    private double coefficient;

    public QuanCountUniqGlycNCoefObject(int uniqueGlycsCount, double coefficient) {
        this.uniqueGlycsCount = uniqueGlycsCount;
        this.coefficient = coefficient;
    }

    public QuanCountUniqGlycNCoefObject(String id, int uniqueGlycsCount, double coefficient) {
        this.id = id;
        this.uniqueGlycsCount = uniqueGlycsCount;
        this.coefficient = coefficient;
    }

    public String getId() {
        return id;
    }

    public double getCoefficient() {
        return coefficient;
    }

    public int getUniqueGlycsCount() {
        return uniqueGlycsCount;
    }
    
    
    
}
