/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package xglycscan.quan.analysis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import xglycscan.quan.QuanCountFileObject;
import xglycscan.quan.QuanCountGroupObject;

/**
 *
 * @author paiyeta1
 */
public class QuanCountFilesAnalyzer {
    
    private ArrayList<QuanCountFileObject> quanCountFileObjs;
    private HashMap<String,QuanCountFileObject> file2QuanCountObjMap;
    private String id = "all_files"; //default collective name
    
    public QuanCountFilesAnalyzer(ArrayList<QuanCountFileObject> quanCountFileObjs, 
            HashMap<String,QuanCountFileObject> file2QuanCountObjMap){
        this.quanCountFileObjs = quanCountFileObjs;
        this.file2QuanCountObjMap = file2QuanCountObjMap;

    }

    public QuanCountFilesAnalyzer(QuanCountGroupObject qCGObj, 
            HashMap<String, QuanCountFileObject> file2QuanCountObjMap) {
        this.id = qCGObj.getGroupName();
        this.quanCountFileObjs = qCGObj.getQfileObjs();
        this.file2QuanCountObjMap = file2QuanCountObjMap;
    }
    
    public QuanCountFilesAnalyzer(String id, ArrayList<QuanCountFileObject> quanCountFileObjs, 
            HashMap<String,QuanCountFileObject> file2QuanCountObjMap){
        this.id = id; 
        this.quanCountFileObjs = quanCountFileObjs;
        this.file2QuanCountObjMap = file2QuanCountObjMap;

    }
    
    
    
    // -- Public Method(s)
    public HashMap<String, QuanCountUniqGlycNCoefObject> getFile2UniqGlyNCoefMap(){
        
        HashMap<String, QuanCountUniqGlycNCoefObject> file2GlycNCoefMap = 
                new HashMap<String, QuanCountUniqGlycNCoefObject>();
        QuanCountGroupObject qCGObj = new QuanCountGroupObject(id, quanCountFileObjs);
        
        HashMap<String, Integer> file2UniqGlycCountMap = qCGObj.calcWithinFileObjUniqGlycs();
        int filesTotalUniqGlycCount = qCGObj.getUniqueGlycSeqs().size();
        
        Set<String> fileIds = file2UniqGlycCountMap.keySet();
        Iterator<String> itr = fileIds.iterator();
        while(itr.hasNext()){
            String fileId = itr.next();
            int fileUniqGlycCount = file2UniqGlycCountMap.get(fileId);
            double fileCoef = calcCoefficient(fileUniqGlycCount, filesTotalUniqGlycCount);
            QuanCountUniqGlycNCoefObject qCUGlycNCoefObj = 
                    new QuanCountUniqGlycNCoefObject(fileUniqGlycCount,fileCoef);
            file2GlycNCoefMap.put(fileId, qCUGlycNCoefObj);            
        }
               
        return file2GlycNCoefMap;        
    }
    
    public QuanSquareMatrix getCountsSimilarityMatrix() {
        QuanSquareMatrix qCSimMatrix = null;
        String[] fileNames = extractFileNames();
        int[][] countsSimMatrix = new int[fileNames.length][fileNames.length];
        for(int i = 0; i < fileNames.length; i++){
            for(int j = 0; j < fileNames.length; j++){
                QuanCountFileObject qcFileObjI = file2QuanCountObjMap.get(fileNames[i]);
                QuanCountFileObject qcFileObjJ = file2QuanCountObjMap.get(fileNames[j]);
                countsSimMatrix[i][j] = qcFileObjI.intersect(qcFileObjJ);
            }
        }
        qCSimMatrix = new QuanSquareMatrix(fileNames,countsSimMatrix);
        return qCSimMatrix;
    }
    
    public QuanSquareMatrix getPercentSimilarityMatrix() {
        QuanSquareMatrix qCSimMatrix = null;
        String[] fileNames = extractFileNames();
        double[][] doubleSimMatrix = new double[fileNames.length][fileNames.length];
        for(int i = 0; i < fileNames.length; i++){
            for(int j = 0; j < fileNames.length; j++){
                QuanCountFileObject qcFileObjI = file2QuanCountObjMap.get(fileNames[i]);
                QuanCountFileObject qcFileObjJ = file2QuanCountObjMap.get(fileNames[j]);
                doubleSimMatrix[i][j] = qcFileObjI.similarity(qcFileObjJ);
            }
        }
        qCSimMatrix = new QuanSquareMatrix(fileNames,doubleSimMatrix);
        return qCSimMatrix;
    }

    
    // -- Private/Helper Method(s) --
    
    private String[] extractFileNames() {
        String[] fileNames = null;
        fileNames = new String[quanCountFileObjs.size()];
        Iterator<QuanCountFileObject> itr = quanCountFileObjs.iterator();
        int index = 0;
        while(itr.hasNext()){
            QuanCountFileObject countFileObj = itr.next();
            fileNames[index] = countFileObj.getFilename();
            index++;
        }
        
        return fileNames;
    }
    
    private double calcCoefficient(int fileUniqGlycCount, int filesTotalUniqGlycCount) {
        double coef = 0;
        coef = (((double) fileUniqGlycCount / (double) filesTotalUniqGlycCount) * 100);        
        return coef;
    }
    
   
     
}
