/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package xglycscan;

import db.Database;
import db.searches.psmPeptide.PSMPeptide;
import java.util.ArrayList;

/**
 *
 * @author paiyeta1
 */
public class XGSFilePeptidePSMExtractor {

   public ArrayList<XGSFilePeptidePSM> extractPeptidePSMs(String psmFile, 
                                                              ArrayList<PSMPeptide> pDPeps,
                                                                  Database dbase) {
        XGSPeptideSequenceFormatter formatter = new XGSPeptideSequenceFormatter();
        ArrayList<XGSFilePeptidePSM> extractedPeptidePSMs = new ArrayList<XGSFilePeptidePSM>();
        //String fileName = new File(psmFile).getName();
        int fileRow = 0;
        for(int i = 0; i < pDPeps.size(); i++){
            fileRow++; //order of the peptide just added unique value to peptide object
            PSMPeptide pep = pDPeps.get(i);
            double evaluationValue = pep.getEvaluationValue();
            // instantiate an extracted FilePeptidePSMObject
            String peptideID = pep.getPeptideID(); //a unique identifier for peptide/PSM  
            double mz = pep.getMz(); // [Da]
            double rT = pep.getrTime(); //[min] retention time	
            int charge = pep.getCharge();
            int scanNumber = pep.getScanNumber();
            
            /*
             * Because input peptides could be of several forms,
             * check if input peptide is a form of (pseudoformatted) formatted peptide
             * if peptide is pseudoformatted/PseudoExtended,
             */
            String exact_sequence = pep.getSequence();
            String extended_sequence = formatter.formatPeptide(exact_sequence.toUpperCase(), dbase);
            extractedPeptidePSMs.add(new XGSFilePeptidePSM(peptideID, 
                                                            fileRow, 
                                                                exact_sequence, 
                                                                    extended_sequence, 
                                                                        evaluationValue,
                                                                            rT, mz, charge, scanNumber ));
        }        
        System.out.println("   " + fileRow + " \"FilePeptidePSM\" objects extracted...");
        return extractedPeptidePSMs;
    }

   
   
}
