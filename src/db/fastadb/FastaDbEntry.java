/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */

package db.fastadb;

import xglycscan.enums.SearchDatabaseType;


/**
 * A single entry in a <code>FASTA</code> database, loaded into memory.  A <code>FastaDbEntry</code>
 * consists of a unique <code>IPI</code> number and an amino acid sequence.
 *
 * <p>
 * (c) Institute for Systems Biology
 *
 * @author	Paul Loriaux
 * @version $Id$
 * @since	1.0
 *
 * file		$RCSFile$
 * created	Thursday, October 07, 2004
 * modified	$Date$
 */


public class FastaDbEntry extends Object{

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
    *																										*
    * 		Attributes																						*
    *																										*
    \* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     * This <code>FastaDbEntry</code> object's header.
     */
    private String header;

    /**
     * This <code>FastaDbEntry</code> object's unique <code>IPI</code> number.
     */
    private int ipi;

    /**
     * The name of this <code>FastaDbEntry</code>.
     */
    private String name;

    /**
     * This <code>FastaDbEntry</code> object's amino acid sequence.
     */
    private String sequence;
    
    private SearchDatabaseType dbtype;

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
    *																										*
    * 		Constructor																						*
    *																										*
    \* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     * Creates a new <code>FastaDbEntry</code> with the given <code>IPI</code> number and amino acid
     * sequence.
     *
     * @param ipi the <code>IPI</code> number that uniquely identifies this <code>FastaDbEntry</code>.
     *
     * @param header the header line for this <code>FastaDbEntry</code>.
     *
     * @param sequence the amino acid sequence of this <code>FastaDbEntry</code>.
     */

    public FastaDbEntry(int ipi, String header, String sequence,SearchDatabaseType dbtype){
        // Create a new Java Object
        super();

        this.ipi = ipi;
        this.header = header;
        this.sequence = sequence;
        this.dbtype = dbtype;
        this.name = parseName();
        
    }


    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
    *																										*
    * 		Methods																							*
    *																										*
    \* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     * Returns the number of times the given <code>String</code> is found within this <code>FastaDbEntry
     * </code> object's sequence.
     *
     * @param str the query <code>String</code>.
     *
     * @return the number of times the given <code>String</code> is found within this <code>FastaDbEntry
     * </code> object's sequence.
     */
    
    int prints = 0;
    
    public int countOccurrencesOf(String str){
        if(prints < 3) System.err.println("Counting occurences of \"" + str + "\" in " + sequence + "...");
        int occ = 0;

        int len = str.length();
        int del_len = sequence.length() - len;
        if(!(del_len < 0)){
            int index = 0;
            while(index < del_len){
                String subseq = sequence.substring( index, index+len );
                if(subseq.equals(str)) occ++;
                if( prints < 3 ) System.err.println( "Comparing \"" + str + "\" to subseq \"" + subseq + "\": " + occ + " occurrences." );
                index++;
            }
        }
        prints++;
        return occ;
    }

    /**
     * Returns this <code>FastaDbEntry</code> object's header.
     *
     * @return this <code>FastaDbEntry</code> object's header.
     */

    public String getHeader(){
        return this.header;
    }

    /**
     * Returns this <code>FastaDbEntry</code> object's unique <code>IPI</code> number.
     *
     * @return this <code>FastaDbEntry</code> object's unique <code>IPI</code> number.
     */
    public int getIpi(){
        return this.ipi;
    }


    /**
     * Returns the name of this <code>FastaDbEntry</code>.
     *
     * @return the name of this <code>FastaDbEntry</code>.
     */
    public String getName(){
        return this.name;
    }

    /**
     * Returns the sequence of this <code>FastaDbEntry</code>.
     *
     * @return the sequence of this <code>FastaDbEntry</code>.
     */
    public String getSequence(){
        return this.sequence;
    }

    /**
     * Returns the index of the specified <code>String</code> within this <code>FastaDbEntry</code>
     * object's sequence, or <code>-1</code> if no such <code>String</code> could be found.
     *
     * @param str the search sequence.
     *
     * @return the index of the specified <code>String</code> within this <code>FastaDbEntry</code>
     * object's sequence, or <code>-1</code> if no such <code>String</code> could be found.
     */
    public int indexOf(String str){
        return sequence.indexOf(str);
    }

    /**
     * Parses out the name of this <code>FastaDbEntry</code> from its header information.
     */
    private String parseName()
    {
        String name = null;
        switch(dbtype){
            case IPIv387:
                name = " 0 ";

                // The name always occurs after the information "TaxId=####"
                int index = header.lastIndexOf("Tax_Id=");

                // If this information doesn't exist, the name can not be parsed
                if(index < 0) return name;

                // Find the next space
                while(header.charAt(index) != ' ') index++;

                // All information from here to the end of the header is the name
                name = header.substring( index+1 ).trim();

                // Replace empty names by a default name
                if( name.equals("") ) name = " 0 ";
                
                break;
            default: //REFSEQ
                String[] lineArr = header.split("\\|");
                name = lineArr[lineArr.length - 1];
                break;                       
                
        }       

        return name;
    }


    /**
        * Returns the <code>String</code> representation of this <code>FastaDbEntry</code>.  This is simply the
        * the <code>IPI</code> number for the entry followed by its amino acid sequence.
        *
        * @return the <code>String</code> representation of this <code>FastaDbEntry</code>.
        */

    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append(FastaDb.ipiToString(ipi,dbtype) + "\n" );
        sb.append(sequence + "\n");
        return sb.toString();
    }


} /* end class FastaDbEntry */