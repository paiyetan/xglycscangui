/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package db.searches.psmPeptide.quan.itraq4plex;

import db.searches.psmPeptide.PSMPeptide;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * the sample-deconvoluter, deconvolutes all identified glycosites into a 
 * rows-versus-columns-like representation the rows represent identified glycosites 
 * while the columns samples in which these are identified.
 * 
 *
 * @author paiyeta1
 */
public class PSMPeptideDataMatrixExtractor{
    
    
    //PhenoDataFileReader reader
    private HashMap<String,String> file2SampleMap;
    private HashMap<String,LinkedList<PSMPeptide>> glyc2IDMap;
    private PSMPeptideDataMatrix matrix; 
    
    public PSMPeptideDataMatrixExtractor(ArrayList<ArrayList<PSMPeptide>> peptides, HashMap<String,String> file2SampleMap){
        
        
        
        this.file2SampleMap = file2SampleMap;
        glyc2IDMap = generateGlycMap(peptides); // maps glycosite to a linked-list of file(s) within which it is found
        matrix = new PSMPeptideDataMatrix(file2SampleMap,glyc2IDMap);
        
        
    }

    private HashMap<String, LinkedList<PSMPeptide>> generateGlycMap(ArrayList<ArrayList<PSMPeptide>> peptides) {
        
        HashMap<String, LinkedList<PSMPeptide>> map = new HashMap<String, LinkedList<PSMPeptide>>();
        
        // iterate through all glycosites derived,
        // map derived glyc(s) to unique instances [emcapsulated in the "DataGlycID" - data glyc identifier, or 'PSMPeptide' object(s)] 
        Iterator<ArrayList<PSMPeptide>> itr = peptides.iterator();
        while(itr.hasNext()){
           ArrayList<PSMPeptide> peps = itr.next(); 
           Iterator<PSMPeptide> itr2 = peps.iterator();
           while(itr.hasNext()){
               PSMPeptide pep = itr2.next();
               String glyc = pep.getGlycosite().getSequence();
               if(map.containsKey(glyc) == false){
                   //DataGlycID glycID = new DataGlycID()
                   LinkedList<PSMPeptide> pepsOfGlyc = new LinkedList<PSMPeptide>();
                   pepsOfGlyc.add(pep);
                   map.put(glyc,pepsOfGlyc);
                   
               } else {
                   LinkedList<PSMPeptide> pepsOfGlyc = map.remove(glyc);
                   pepsOfGlyc.add(pep);
                   map.put(glyc,pepsOfGlyc);                  
               }
           }
        }
            
        return map;    
    }

    public HashMap<String, LinkedList<PSMPeptide>> getGlyc2IDMap() {
        return glyc2IDMap;
    }

    public PSMPeptideDataMatrix getMatrix() {
        return matrix;
    }
    
    
}
