/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package db.searches.psmPeptide.quan.itraq4plex;

import db.searches.psmPeptide.PSMPeptide;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

/**
 *
 * @author paiyeta1
 */
public class PSMPeptideDataMatrix {
    
    private String[] glycosites;
    private String[] sampleNames;
    private double[][] matrix;
    
    
    public PSMPeptideDataMatrix(HashMap<String, String> file2SampleMap, 
            HashMap<String, LinkedList<PSMPeptide>> glyc2IDMap) {
        //throw new UnsupportedOperationException("Not yet implemented");
        setGlycArray(glyc2IDMap);
        setSampleNames(file2SampleMap);
        matrix = new double[glycosites.length][sampleNames.length];
        //initialize array
        for(int i = 0; i < glycosites.length; i++){
            for(int j = 0; j <sampleNames.length; j ++){
                matrix[i][j] = 0;
            }
        }
        populateMatrix(glyc2IDMap);
        
        
    }

    private void setGlycArray(HashMap<String, LinkedList<PSMPeptide>> glyc2IDMap) {
        //throw new UnsupportedOperationException("Not yet implemented");
        Set<String> glycs = glyc2IDMap.keySet();
        glycosites = new String[glycs.size()];
        Iterator<String> itr = glycs.iterator();
        int index = 0;
        while(itr.hasNext()){
            String glyc = itr.next();
            glycosites[index] = glyc;
            index++;           
        }
        
    }

    private void setSampleNames(HashMap<String, String> file2SampleMap) {
        //throw new UnsupportedOperationException("Not yet implemented");
        Set<String> fileNChannels = file2SampleMap.keySet();
        sampleNames = new String[fileNChannels.size()];
        Iterator<String> itr = fileNChannels.iterator();
        int index = 0;
        while(itr.hasNext()){
            String fileNChannel = itr.next();
            sampleNames[index] = fileNChannel;
            index++;           
        }
    }

    private void populateMatrix(HashMap<String, LinkedList<PSMPeptide>> glyc2IDMap) {
        //throw new UnsupportedOperationException("Not yet implemented");
        for(int i = 0; i < glycosites.length; i++){
            for(int j = 0; j < sampleNames.length; j++){
                String glyc = glycosites[i];
                String fileNChannelID = sampleNames[j];
                String[] fileChannelIDArr = fileNChannelID.split("\\.");
                
                String channelID = fileChannelIDArr[fileChannelIDArr.length - 1];
                String file = fileChannelIDArr[0];
                double channelValue = 0;
                
                LinkedList<PSMPeptide> peps = glyc2IDMap.get(glyc);
                Iterator<PSMPeptide> itr = peps.iterator();
                while(itr.hasNext()){
                    PSMPeptide pep = itr.next();
                    if(file.equalsIgnoreCase(pep.getSpecFile().replace(".raw", ""))){
                        channelValue = getChannelIntensity(channelID,pep);
                        break;
                    }
                }
                //double channel_value
                matrix[i][j] = channelValue;
            }
        }
    }

    private double getChannelIntensity(String channelID, PSMPeptide pep) {
        // throw new UnsupportedOperationException("Not yet implemented");
        double channelIntensity = 0;
        if(channelID.equalsIgnoreCase("114")){
            channelIntensity = pep.getItraq_114();
        }else if(channelID.equalsIgnoreCase("115")){
            channelIntensity = pep.getItraq_115();
        }else if(channelID.equalsIgnoreCase("116")){
            channelIntensity = pep.getItraq_116();
        }else if(channelID.equalsIgnoreCase("117")){
            channelIntensity = pep.getItraq_117();       
        }        
        return channelIntensity;
    }

    public String[] getFileNChannelIDs() {
        return sampleNames;
    }

    public String[] getGlycosites() {
        return glycosites;
    }

    public double[][] getMatrix() {
        return matrix;
    }
    
    
    
    
}
