/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package db.searches.psmPeptide.quan.itraq4plex;

import db.searches.psmPeptide.PSMPeptide;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author paiyeta1
 */
public class PSMPeptideDataGlycSummarizer {
    
    public ArrayList<PSMPeptide> summarize(ArrayList<PSMPeptide> filteredPDPeps){
        
        ArrayList<PSMPeptide> summarized = new ArrayList<PSMPeptide>();
        Iterator<PSMPeptide> itr = filteredPDPeps.iterator();
        int peptideIndex = 0;
        /*  iterate through peptides,
            for each peptide (glycosite) object, 
                - get similar peptides in terms of sequence
                - as you get this similar peptides, remove their instances from the list,
            - summarize values of these into a new peptide object and 
            - put this into the summarized list object
         * 
         */
        while(itr.hasNext()){
            PSMPeptide peptide = itr.next();
            String glycSequence = peptide.getGlycosite().getSequence();
            ArrayList<PSMPeptide> similarPeptides = new ArrayList<PSMPeptide>();
            similarPeptides.add(peptide);
            for(int i = peptideIndex + 1; i < filteredPDPeps.size(); i++){
                if(filteredPDPeps.get(i).getGlycosite().getSequence().equalsIgnoreCase(glycSequence)){
                    similarPeptides.add(filteredPDPeps.get(i));
                    filteredPDPeps.remove(i);
                    i--;
                }
            }
            PSMPeptide pep = summarizePDPepValues(similarPeptides);
            summarized.add(pep);
            
            
        }
        
        
        return summarized;
    }

    private PSMPeptide summarizePDPepValues(ArrayList<PSMPeptide> similarPeptides) {
        //throw new UnsupportedOperationException("Not yet implemented");
        PSMPeptide pep;
        /* 
         * values to summarize
         *  String conf; //peptide confidence
            String sequence;	
            String protDescr; //protein description	
            int proteins; //no of proteins mapped to	
            int proteinGrps; //no of protein groups mapped to
            String[] protGrpAccns;
            String[] modifxns;	
            double deltaScore;	
            int rank;	
            int sEngineRank;	
            double itraq_114;	
            double itraq_115;	
            double itraq_116;	
            double itraq_117;	
            double xCorr;	
            double spScore;
            int mCleavs; //missed cleavages	
            double intensity;	
            int charge;	
            double mz; // [Da]	
            double rTime; //[min] retention time	
            int matIons; //matched ions	
            int totIons; //total ions	
            String specFile; //spectrum file
            double probability;
            Glycosite glycosite = null;

         */
        double itraq_114 = average(similarPeptides, "114");
        double itraq_115 = average(similarPeptides, "115");
        double itraq_116 = average(similarPeptides, "116");
        double itraq_117 = average(similarPeptides, "117");
        double intensity = average(similarPeptides, "intensity");
        PSMPeptide best = prioritize(similarPeptides, "rank"); //for 
        
        pep = new PSMPeptide(best.getPeptideID(), best.getSequence(), 
                        //best.getProtDescr(), 
                            //best.getProteins(), best.getProteinGrps(), best.getProtGrpAccns(), 
                                best.getModifxns(), 
                                    //best.getDeltaScore(), best.getRank(), 
                                    best.getsEngineRank(), 
                                    itraq_114, itraq_115, itraq_116, itraq_117, 
                                        best.getxCorr(), best.getSpScore(), best.getmCleavs(), intensity, best.getCharge(), 
                                            best.getMz(), best.getrTime(), best.getMatIons(), best.getTotIons(), best.getSpecFile(),
                                                best.getEvaluationValue(),
                                                    best.getScanNumber());
        pep.setGlycosite(best.getGlycosite());
        return pep;
    }

    private double average(ArrayList<PSMPeptide> similarPeptides, String channel) {
        //throw new UnsupportedOperationException("Not yet implemented");
        double av = 0;
        ArrayList<Double> values = new ArrayList<Double>();
        if(channel.equalsIgnoreCase("114")){
            for(int i = 0; i < similarPeptides.size(); i++){
                values.add(similarPeptides.get(i).getItraq_114());
            }
            double value_sum = sumValues(values);
            av = value_sum/similarPeptides.size();
        }else if(channel.equalsIgnoreCase("115")){
            for(int i = 0; i < similarPeptides.size(); i++){
                values.add(similarPeptides.get(i).getItraq_115());
            }
            double value_sum = sumValues(values);
            av = value_sum/similarPeptides.size();
        }else if(channel.equalsIgnoreCase("116")){
            for(int i = 0; i < similarPeptides.size(); i++){
                values.add(similarPeptides.get(i).getItraq_116());
            }
            double value_sum = sumValues(values);
            av = value_sum/similarPeptides.size();
        }else if(channel.equalsIgnoreCase("117")){
            for(int i = 0; i < similarPeptides.size(); i++){
                values.add(similarPeptides.get(i).getItraq_117());
            }
            double value_sum = sumValues(values);
            av = value_sum/similarPeptides.size();
        }else if(channel.equalsIgnoreCase("intensity")){
            for(int i = 0; i < similarPeptides.size(); i++){
                values.add(similarPeptides.get(i).getIntensity());
            }
            double value_sum = sumValues(values);
            av = value_sum/similarPeptides.size();
        }
        return av;
    }

    private PSMPeptide prioritize(ArrayList<PSMPeptide> similarPeptides, String rank) {
        //throw new UnsupportedOperationException("Not yet implemented");
        PSMPeptide best = similarPeptides.get(0);
        for(int i = 1; i < similarPeptides.size(); i++){
            if(similarPeptides.get(i).getsEngineRank() < best.getsEngineRank()){
                best = similarPeptides.get(i);
            }
        }        
        return best;
    }

    private double sumValues(ArrayList<Double> values) {
        //throw new UnsupportedOperationException("Not yet implemented");
        double sum = 0;
        for(int i = 0; i < values.size(); i++){
            sum = sum + values.get(i);
        }
        return sum;
    }
    
}
