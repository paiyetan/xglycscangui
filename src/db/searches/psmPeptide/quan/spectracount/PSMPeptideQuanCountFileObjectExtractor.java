/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package db.searches.psmPeptide.quan.spectracount;

import db.searches.psmPeptide.PSMPeptide;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import xglycscan.quan.QuanCountFileObject;

/**
 *
 * @author paiyeta1
 */
public class PSMPeptideQuanCountFileObjectExtractor {
    
    public QuanCountFileObject extractQuanObject(String fName, ArrayList<PSMPeptide> mappedPeps){
        
        QuanCountFileObject quanObj = null;
        /*
           quanObj properties include: 
               String filename; 
               ArrayList<String> uniqueGlycSeqs; 
               ArrayList<FileUniqueGlycosite> uniqueGlycosites;
         
        * 
        */
        String filename = fName;
        
        ArrayList<String> uniqueGlycSeqs = extractUniqueGlycositeSeqs(mappedPeps); 
        System.out.println("   " + uniqueGlycSeqs.size() + " unique glycosite sequence(s) found...");
        
        HashMap<String, Integer> glycSeq2CountMap = extractUniqueGlyc2CountMap(uniqueGlycSeqs, mappedPeps);
        System.out.println("   " + glycSeq2CountMap.size() + " unique glycosite(s) mapped to respective counts...");
        
        quanObj = new QuanCountFileObject(filename, uniqueGlycSeqs, glycSeq2CountMap);    
        
        return quanObj;
        
    }

    private ArrayList<String> extractUniqueGlycositeSeqs(ArrayList<PSMPeptide> mappedPeps) {
        
        ArrayList<String> uniqGlycSeqs = new ArrayList<String>();
        Iterator<PSMPeptide> itr = mappedPeps.iterator();
        
        while(itr.hasNext()){
            PSMPeptide pep = itr.next();
            String sequence = pep.getGlycosite().getSequence();
            if(uniqGlycSeqs.contains(sequence) == false){
                uniqGlycSeqs.add(sequence);
            }
        }        
        return uniqGlycSeqs;
        
    }

    private HashMap<String, Integer> extractUniqueGlyc2CountMap(ArrayList<String> uniqueGlycSeqs, ArrayList<PSMPeptide> mappedPeps) {
        
        HashMap<String, Integer> fileUniqGlycs2CountMap = new HashMap<String, Integer>();
        
        //for each unique Glycosite sequence, get occurrence and instantiate 
        Iterator<String> itr = uniqueGlycSeqs.iterator();
        while(itr.hasNext()){
            String glycSeq = itr.next();
            int count = getGlycSeqCount(glycSeq,mappedPeps);
            fileUniqGlycs2CountMap.put(glycSeq,count);
        }
        return fileUniqGlycs2CountMap;
        
    }

    private int getGlycSeqCount(String glycSeq, ArrayList<PSMPeptide> mappedPeps) {
        
        int count = 0;
        Iterator<PSMPeptide> itr = mappedPeps.iterator();
        while(itr.hasNext()){
            PSMPeptide pep = itr.next();
            if(pep.getGlycosite().getSequence().equalsIgnoreCase(glycSeq)){
                count++;
            }
        }       
        return count;
        
    }
    
}
