/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *      Copyright (c) 2014, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */

package db.searches.psmPeptide;

import xglycscan.XGSGlycosite;



/**
 * 
 * @author paiyeta1
 */
public class PSMPeptide implements Comparable<PSMPeptide>{
	
    private String peptideID; //peptide confidence
    private String sequence;	
    
    private String[] modifxns;	
    
    private int sEngineRank;	
    private double itraq_114;	
    private double itraq_115;	
    private double itraq_116;	
    private double itraq_117;	
    private double xCorr;	
    private double spScore;
    private int mCleavs; //missed cleavages	
    private double intensity;	
    private int charge;	
    private double mz; // [Da]	
    private double rTime; //[min] retention time	
    private int matIons; //matched ions	
    private int totIons; //total ions	
    private String specFile; //spectrum file
    
    private double evaluationValue;
    private XGSGlycosite glycosite = null;
    
    private final int VALUE_114 = 1;
    private final int VALUE_115 = 2;
    private final int VALUE_116 = 3;
    private final int VALUE_117 = 4;
    
    //
    private boolean isDecoy = false;
    private double pValue;
    private double fDR;
    private double qValue;
    
    // include scan number in peptide
    private int scanNumber; //typically the first scan number, analogous to the associated scan's native ID...

    
    /**
     * 
     * @param peptideID
     * @param sequence
     * @param modifxns
     * @param sEngineRank
     * @param itraq_114
     * @param itraq_115
     * @param itraq_116
     * @param itraq_117
     * @param xCorr
     * @param spScore
     * @param mCleavs
     * @param intensity
     * @param charge
     * @param mz
     * @param rTime
     * @param matIons
     * @param totIons
     * @param specFile
     * @param evaluationValue
     * @param firstscan
     */
    public PSMPeptide(String peptideID, String sequence, 
                            //String protDescr, 
                            //int proteins, int proteinGrps, String[] protGrpAccns, 
                                String[] modifxns, 
                                //double deltaScore, int rank, 
                                int sEngineRank, 
                                    double itraq_114, double itraq_115, double itraq_116, double itraq_117, 
                                        double xCorr, double spScore, int mCleavs, double intensity, int charge, 
                                            double mz, double rTime, int matIons, int totIons, String specFile, 
                                                double evaluationValue, int firstscan) {
        this.peptideID = peptideID;
        this.sequence = sequence;
        //this.protDescr = protDescr;
        //this.proteins = proteins;
        //this.proteinGrps = proteinGrps;
        //this.protGrpAccns = protGrpAccns;
        this.modifxns = modifxns;
        //this.deltaScore = deltaScore;
        //this.rank = rank;
        this.sEngineRank = sEngineRank;
        this.itraq_114 = itraq_114;
        this.itraq_115 = itraq_115;
        this.itraq_116 = itraq_116;
        this.itraq_117 = itraq_117;
        this.xCorr = xCorr;
        this.spScore = spScore;
        this.mCleavs = mCleavs;
        this.intensity = intensity;
        this.charge = charge;
        this.mz = mz;
        this.rTime = rTime;
        this.matIons = matIons;
        this.totIons = totIons;
        this.specFile = specFile;
        this.evaluationValue = evaluationValue;
        this.scanNumber = firstscan; //scan number
        //setEvaluationValue(peptideID);
    }

    /**
    * @param pep
    * @return
    */
    public boolean equalInSeqNModfxns(PSMPeptide pep){
        boolean equal = false;
        if(equalInSequence(pep) && equalInModfxns(pep)){
            equal = true;
        }
        return equal;
    }
    /**
    * 
    * @param pep
    * @return
    */
    public boolean equalInSequence(PSMPeptide pep){
        boolean equal = false;
        String sequence = this.getSequence().toUpperCase();
        if(sequence.equals(pep.getSequence().toUpperCase())){
            equal = true;
        }
        return equal;
    }

    /**
    * @param pep
    * @return
    */
    public boolean equalInModfxns(PSMPeptide pep){
        boolean equal = false;
        String[] modfxns = pep.getModifxns();
        if(this.getModifxns().length == modfxns.length && sameModfxns(modfxns,this.getModifxns())){
            equal = true;
        }
        return equal;
    }

    /**
    * @param modfxns
    * @param modifxns2
    * @return
    * 
    * Modifications can only be true (the same) if and only if the same 
    * modifications occur at similar position/sequence 
    */
    private boolean sameModfxns(String[] modfxns, String[] modifxns2) {
        boolean sameMod = true;
        for(int i = 0; i<modfxns.length; i++){
            if(!modfxns[i].equals(modifxns2[i])){
                    sameMod = false;
            }
        }
        return sameMod;
    }

    /**
     * 
     * @return
     */
    public int getCharge() {
        return charge;
    }

    /**
     * 
     * @return
     */
    public String getPeptideID() {
        return peptideID;
    }

    /*
     public double getDeltaScore() {
        return deltaScore;
    }
    * 
    */

    /**
     * 
     * @return
     */
    public double getIntensity() {
        return intensity;
    }

    /**
     * 
     * @return
     */
    public double getItraq_114() {
        return itraq_114;
    }

    /**
     * 
     * @return
     */
    public double getItraq_115() {
        return itraq_115;
    }

    /**
     * 
     * @return
     */
    public double getItraq_116() {
        return itraq_116;
    }

    /**
     * 
     * @return
     */
    public double getItraq_117() {
        return itraq_117;
    }

    /**
     * 
     * @return
     */
    public int getmCleavs() {
        return mCleavs;
    }

    /**
     * 
     * @return
     */
    public int getMatIons() {
        return matIons;
    }

    /**
     * 
     * @return
     */
    public String[] getModifxns() {
        return modifxns;
    }

    /**
     * 
     * @return
     */
    public double getMz() {
        return mz;
    }
    /*
    public String getProtDescr() {
        return protDescr;
    }

    public String[] getProtGrpAccns() {
        return protGrpAccns;
    }

    public int getProteinGrps() {
        return proteinGrps;
    }

    public int getProteins() {
        return proteins;
    }
    * 
    */

    /**
     * 
     * @return
     */
    public double getrTime() {
        return rTime;
    }

    /*
    public int getRank() {
        return rank;
    }
    * 
    */

    /**
     * 
     * @return
     */
    public int getsEngineRank() {
        return sEngineRank;
    }

    /**
     * 
     * @return
     */
    public String getSequence() {
        return sequence;
    }

    /**
     * 
     * @return
     */
    public double getSpScore() {
        return spScore;
    }

    /**
     * 
     * @return
     */
    public String getSpecFile() {
        return specFile;
    }

    /**
     * 
     * @return
     */
    public int getTotIons() {
        return totIons;
    }

    /**
     * 
     * @return
     */
    public double getxCorr() {
        return xCorr;
    }

    /**
     * 
     * @return
     */
    public XGSGlycosite getGlycosite() {
        return glycosite;
    }

    
    /**
     * 
     * @return
     */
    public double getEvaluationValue() {
        return evaluationValue;
    }

    /**
     * 
     * @return
     */
    public double getfDR() {
        return fDR;
    }

    /**
     * 
     * @return
     */
    public double getpValue() {
        return pValue;
    }

    /**
     * 
     * @return
     */
    public double getqValue() {
        return qValue;
    }

    /**
     * 
     * @return
     */
    public int getScanNumber() {
        return scanNumber;
    }
     
    /**
     * 
     * @return
     */
    public boolean isIsDecoy(){
        return isDecoy;
    }
    
    
    /**
     * 
     * @param glycosite
     */
    public void setGlycosite(XGSGlycosite glycosite) {
        this.glycosite = glycosite;
    }
    
    /**
     * 
     */
    public void setAsDecoy(){
        isDecoy = true;
    }
    
    /**
     * 
     * @param pval
     */
    public void setPValue(double pval){
        this.pValue = pval;
    }
    
    /**
     * 
     * @param fDR
     */
    public void setFDR(double fDR){
        this.fDR = fDR;
    }
    
    /**
     * 
     * @param qval
     */
    public void setQValue(double qval){
        this.qValue = qval;
    }
   
        
    /**
     * 
     * @param channel
     * @param channel_coefficient
     * @param base_coefficient
     */
    public void normalizeChannel(int channel, double channel_coefficient, double base_coefficient) {
        //throw new UnsupportedOperationException("Not yet implemented");
        switch(channel){
            case VALUE_115:
                itraq_115 = (itraq_115/channel_coefficient) * base_coefficient; 
                break;
            case VALUE_116:
                itraq_116 = (itraq_116/channel_coefficient) * base_coefficient; 
                break;
            case VALUE_117:
                itraq_117 = (itraq_117/channel_coefficient) * base_coefficient; 
                break;
            default:
                break;
                
        }
    }

    @Override
    public int compareTo(PSMPeptide pep) {
        int compareValue = 0;
        double pepEvalValue = pep.getEvaluationValue();
        //descending order
        if(pepEvalValue < this.evaluationValue){
            compareValue =  -1;
        } else if(pepEvalValue > this.evaluationValue){
            compareValue =  1;
        }
        return compareValue;
        
        /*
        //ascending order
        if(this.evaluationValue < pepEvalValue){
            compareValue =  -1;
        } else if(this.evaluationValue > pepEvalValue){
            compareValue =  1;
        }
        return compareValue;
        * 
        */
    }
        

}
